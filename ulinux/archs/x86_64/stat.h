#ifndef ULINUX_ARCH_STAT_H
#define ULINUX_ARCH_STAT_H
/*******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND (sylvain.bertrand AT gmail dot com)
*******************************************************************************/
struct ulinux_stat{
  ulinux_ul dev;
  ulinux_ul ino;
  ulinux_ul nlink;

  ulinux_u  mode;
  ulinux_u  uid;
  ulinux_u  gid;
  ulinux_u  __pad0;
  ulinux_ul rdev;
  ulinux_l  sz;
  ulinux_l  blk_sz;
  ulinux_l  blks;/*number 512-byte blocks allocated*/

  ulinux_ul atime;
  ulinux_ul atime_nsec;
  ulinux_ul mtime;
  ulinux_ul mtime_nsec;
  ulinux_ul ctime;
  ulinux_ul ctime_nsec;

  ulinux_l  __unused[3];
};
#endif
