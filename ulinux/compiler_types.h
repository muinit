#ifndef ULINUX_COMPILER_TYPES_H
#define ULINUX_COMPILER_TYPES_H
/******************************************************************************
this code is protected by the GNU affero GPLv3
author:Sylvain BERTRAND <sylvain.bertrand AT gmail dot com>
*******************************************************************************/
/*
with GCC
'char' is not a 'signed char' neither it is a 'unsigned char'
but 'char' is signed on x86 and unsigned on ARM... feel the difference
typedef signed char        ulinux_t; t stands for tiny
typedef short              ulinux_s;
typedef int                ulinux_i;
typedef long               ulinux_l;
typedef long long          ulinux_ll;

typedef unsigned char      ulinux_ut; t stands for tiny
typedef unsigned short     ulinux_us;
typedef unsigned           ulinux_u;
typedef unsigned long      ulinux_ul;
typedef unsigned long long ulinux_ull;

typedef float ulinux_f;
*/

#define ulinux_t signed char
#define ulinux_s short
#define ulinux_i int
#define ulinux_l long
#define ulinux_ll long long

#define ulinux_ut unsigned char
#define ulinux_us unsigned short
#define ulinux_u unsigned
#define ulinux_ul unsigned long
#define ulinux_ull unsigned long long

#define ulinux_f float
#endif
