#ifndef OUT_H
#define OUT_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND <sylvain.bertrand AT legeek DOT net>
 */
#ifndef QUIET
	#include <stdarg.h>
	#include <ulinux/utils/mem.h>
	#include <ulinux/utils/ascii/string/vsprintf.h>
	
	#define PRE "muinit:"
	#define DPRINTF_BUF_SZ 1024

	#ifdef INIT_C
		static ulinux_u8 g_dprintf_buf[DPRINTF_BUF_SZ];
	#else
		extern ulinux_u8 *g_dprintf_buf;
	#endif

	#define OUT(f, ...) ulinux_dprintf(0, g_dprintf_buf, DPRINTF_BUF_SZ, (ulinux_u8*)(f), ##__VA_ARGS__)
#else
	#define PRE
	#define OUT(f, ...)
#endif
/* vim: set ts=4 sw=0 noexpandtab: */
#endif
