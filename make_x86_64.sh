#!/bin/sh
build_dir=$(readlink -f .)
printf "BUILD_DIR=$build_dir\n"
src_dir=$(dirname $(readlink -f $0))
printf "SRC_DIR=$src_dir\n"

cpp=/opt/toolchains/x64/elf/binutils-gcc/current/bin/cpp
cc=/opt/toolchains/x64/elf/binutils-gcc/current/bin/gcc
ld=/opt/toolchains/x64/elf/binutils-gcc/current/bin/ld

rm -Rf $build_dir/ulinux
mkdir -p $build_dir/ulinux
ln -sTf $src_dir/ulinux/archs/x86_64 $build_dir/ulinux/arch

# Those are the ulinux files to build for a non-quiet/talkative init:
#  - ulinux/utils/mem.c
#  - utils/ascii/string/vsprintf.c
printf 'CPP init.c->init.cpp.c\n'
$cpp -DQUIET -DNO_TTY -I$build_dir -I$src_dir -o $build_dir/init.cpp.c $src_dir/init.c
printf 'CC init.cpp.c->init.cpp.c.o\n'
$cc -O0 -c -o $build_dir/init.cpp.c.o $build_dir/init.cpp.c
printf 'LD init.cpp.c.o->init\n'
$ld -nostdlib -s -o $build_dir/init $build_dir/init.cpp.c.o

