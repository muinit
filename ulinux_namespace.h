#ifndef ULINUX_NAMESPACE_H
#define ULINUX_NAMESPACE_H
#define rt_sigprocmask(a,b,c) ulinux_sysc(rt_sigprocmask,4,a,b,0,c)
#define SIG_BLOCK               ULINUX_SIG_BLOCK
#define SIG_UNBLOCK             ULINUX_SIG_UNBLOCK
#define SIGCHLD                 ULINUX_SIGCHLD
#define exit_group(a)           ulinux_sysc(exit_group,1,a);unreachable();
#define ISERR                   ULINUX_ISERR
#define clone(a)                ulinux_sysc(clone,5,a,0,0,0,0)
#define execve(a,b)             ulinux_sysc(execve,4,a,b,0,0)
#define setsid(a)               ulinux_sysc(setsid,0)
#define waitid(a,b,c,d)         ulinux_sysc(waitid,5,a,b,c,d,0)
#define P_PID                   ULINUX_P_PID
#define siginfo                 ulinux_siginfo
#define P_ALL                   ULINUX_P_ALL
#define WEXITED                 ULINUX_WEXITED
#define WALL			ULINUX_WALL
#define u8                      ulinux_u8
#define i                       int
#define l                       long
#define ul                      unsigned long
#define loop                    for(;;)
#endif
